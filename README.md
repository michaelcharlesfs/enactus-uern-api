# Opentravel

This project was generated with [ExpressJS Generator](http://expressjs.com/pt-br/starter/generator.html) version 4.15.2.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files. If this does not happen, make sure the [nodemon](https://nodemon.io) is installed in the package.json file. To install the nodemon, run `npm install nodemon --save` and wait a few seconds for the installation to complete.

## Further help

If you need more help, contact developer (Michael Charles)[michaelcharlesfs@gmail.com].

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/michaelcharlesfs/role-based-angular/tags).

## Authors

* **Michael Charles** - [GitLab](https://gitlab.com/michaelcharlesfs)

## Version

Version 0.0.2