var express = require('express');
var router = express.Router();
var db = require('../database');
var guard = require('./guard');
var Joi = require('joi');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

const schema = Joi.object().keys({
  name: Joi.string().required(),
  initials: Joi.string().required(),
  cnpj: Joi.string().required()
});

const collectPointSchema = Joi.object().keys({
    idCollectPoint: Joi.string().required()
});

router.get('/', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    db.query(`FOR c IN cooperative RETURN c`, function (err, cursor) {
      if (err) {
        res.status(500).send('Internal Server Error');
      } else {
        cursor.all(function (err, result) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            res.status(200).send(result);
          }
        });
      }
    });
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id; 
    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN cooperative FILTER c._id == @idCooperative RETURN c`, { idCooperative: `cooperative/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result[0]);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.post('/', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    const validate = Joi.validate(body, schema);

    if (validate.error) {
      res.status(400).send(validate.error.toString());
    } else {
      db.query(`
        INSERT @cooperative INTO cooperative 
          LET cooperativeAdded = NEW 
          RETURN cooperativeAdded`, { cooperative: body }, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(201).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.put('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    let id = req.params.id;
    const validate = Joi.validate(body, schema);

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      if (validate.error) {
        res.status(400).send(validate.error.toString());
      } else {
        db.query(`
          FOR c IN cooperative
            FILTER c._id == @idCooperative
              UPDATE c WITH @cooperative INTO cooperative
                RETURN NEW
        `, {
          idCooperative: 'cooperative/' + id,
          cooperative: body
        }, function (err, cursor) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            cursor.all(function (err, result) {
              if (err) {
                res.status(500).send('Internal Server Error');
              } else {
                res.status(201).send(result);
              }
            });
          }
        });
      }
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.delete('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id;

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      db.query(`
          REMOVE { _key: @idCooperative } IN cooperative
            LET cooperativeRemoved = OLD
            RETURN cooperativeRemoved`, { idCooperative: id }, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.post('/:id/collectPoints', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    let id = req.params.id;
    const validate = Joi.validate(body, collectPointSchema);

    if (validate.error) {
      res.status(400).send(validate.error.toString());
    } else {
      db.query(`
        INSERT { _from: @idCooperative, _to: @idCollectPoint } INTO hasCooperativeCollectPoint 
          LET edge = NEW 
          RETURN edge`, { idCooperative: `cooperative/${id}`, idCollectPoint: `collectPoint/${body.idCollectPoint}` }, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(201).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id/collectPoints', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id; 
    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN ANY @idCooperative hasCooperativeCollectPoint RETURN c`, { idCooperative: `cooperative/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id/farmers', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id; 
    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN ANY @idCooperative hasCooperativeFarmer RETURN c`, { idCooperative: `cooperative/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id/products', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id; 
    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN ANY @idCooperative hasCooperativeProduct RETURN c`, { idCooperative: `cooperative/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

module.exports = router;
