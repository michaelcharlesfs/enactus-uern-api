var express = require('express');
var router = express.Router();

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var db = require('../database');
var Joi = require('joi');

const schema = Joi.object().keys({
  user: Joi.string().alphanum().required(),
  password: Joi.string().required()
});

router.post('/login', function (req, res, next) {
  let body = req.body;
  const validate = Joi.validate(body, schema);

  if (validate.error) {
    res.status(400).send(validate.error.toString());
  } else {
    db.query(`FOR u IN user FILTER u.user == @user RETURN u`, { user: body.user }, function (err, cursor) {
      if (err) {
        res.status(500).send({ auth: false, message: 'Internal Server Error' });
      } else {
        cursor.all(function (err, result) {
          if (err) {
            res.status(500).send({ auth: false, message: 'Internal Server Error' });
          } else {
            if (result.length >= 1) {
              let validPassword = bcrypt.compareSync(body.password, result[0].password);
              if (!validPassword) {
                res.status(201).send({ auth: false, message: 'Password Invalid', code: 3 });
              } else {
                if (!result[0].status) {
                  res.status(201).send({ auth: false, message: 'Inactive user', code: 4 });
                } else {
                  let token = jwt.sign({ id: result[0]._key, permission: result[0].permission }, process.env.secret, { expiresIn: 3600 });
                  res.status(200).send({ auth: true, token: token, code: 1 });
                }
              }
            } else {
              res.status(201).send({ auth: false, message: 'No User Found', code: 2 })
            }
          }
        });
      }
    });
  }
});

router.get('/logout', function (req, res, next) {
  res.status(200).send({ auth: false, token: null });
});

module.exports = router;
