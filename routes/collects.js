var express = require('express');
var router = express.Router();
var db = require('../database');
var guard = require('./guard');
var Joi = require('joi');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

const schema = Joi.object().keys({
  status: Joi.string().required(),
  date: Joi.string().empty(''),
  idCollectPoint: Joi.string().required()
});

router.get('/', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    db.query(`FOR c IN collect RETURN c`, function (err, cursor) {
      if (err) {
        res.status(500).send('Internal Server Error');
      } else {
        cursor.all(function (err, result) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            res.status(200).send(result);
          }
        });
      }
    });
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id;

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN collect FILTER c._id == @idCollect RETURN c`, { idCollect: `collect/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result[0]);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.post('/', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    const validate = Joi.validate(body, schema);

    if (validate.error) {
      res.status(400).send(validate.error.toString());
    } else {
      let idCollectPoint = body.idCollectPoint;
      delete body.idCollectPoint;
      db.query(`
        INSERT @collect INTO collect 
          LET collectAdded = NEW
          LET newEdge = (INSERT { _from: @idCollectPoint, _to: collectAdded._id } IN hasCollectPointCollect RETURN NEW) RETURN collectAdded`, { collect: body, idCollectPoint: `collectPoint/${idCollectPoint}` }, function (err, cursor) {
        if (err) {
          console.log(err);
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(201).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.put('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    let id = req.params.id;
    const validate = Joi.validate(body, schema);

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      if (validate.error) {
        res.status(400).send(validate.error.toString());
      } else {
        db.query(`
          FOR c IN collect
            FILTER c._id == @idCollect
              UPDATE c WITH @collect INTO collect
                RETURN NEW
        `, {
          idCollect: 'collect/' + id,
          collect: body
        }, function (err, cursor) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            cursor.all(function (err, result) {
              if (err) {
                res.status(500).send('Internal Server Error');
              } else {
                res.status(201).send(result);
              }
            });
          }
        });
      }
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.delete('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id;

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      db.query(`REMOVE { _key: @idCollect } IN collect RETURN OLD`, {idCollect: id}, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

module.exports = router;
