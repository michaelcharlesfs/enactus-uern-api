var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

let guard = {};

guard.authorizate = ((token, permissionRequired) => {
    let response = {auth: false};
    if (!token) {
        response.message = 'No token provided!';
    } else {
        jwt.verify(token, process.env.secret, (err, decoded) => {
            if (err) {
                if (err.message === 'jwt expired') {
                    response.message = 'Session expired';
                    response.code = 5;
                } else {
                    response.message = err.message; 
                    response.code = 6 ;
                }
            } else {
                console.log(decoded);
                if (decoded.permission === permissionRequired) {
                    response.auth = true;
                    response.token = token;
                } else {
                    response.message = 'Not authorized';
                    response.code = 401;
                }
            }
        });     
    }
    return response;
});

module.exports = guard;

  