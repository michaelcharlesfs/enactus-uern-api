var express = require('express');
var router = express.Router();
var db = require('../database');
var guard = require('./guard');
var Joi = require('joi');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

const schema = Joi.object().keys({
  name: Joi.string().required(),
  lat: Joi.string().required(),
  lng: Joi.string().required(),
  idCooperative: Joi.string().required()
});

router.get('/', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    db.query(`FOR f IN farmer RETURN f`, function (err, cursor) {
      if (err) {
        res.status(500).send('Internal Server Error');
      } else {
        cursor.all(function (err, result) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            res.status(200).send(result);
          }
        });
      }
    });
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id; 
    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`
        FOR f IN farmer FILTER f._id == @idFarmer
          LET cooperative = (FOR c IN ANY f._id hasCooperativeFarmer RETURN c)
          LET collectPoints = (FOR c IN ANY f._id hasCollectPointFarmer RETURN c)
          RETURN {farmer: f, cooperative: cooperative[0], collectPoints: collectPoints}`, { idFarmer: `farmer/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result[0]);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.post('/', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    const validate = Joi.validate(body, schema);

    if (validate.error) {
      res.status(400).send(validate.error.toString());
    } else {
      let idCollectPoint = body.idCollectPoint;
      let idCooperative = body.idCooperative;
      delete body.idCollectPoint;
      delete body.idCooperative;
      db.query(`
        INSERT @farmer INTO farmer 
          LET farmerAdded = NEW
          LET newEdge = (INSERT { _to: @idCollectPoint, _from: farmerAdded._id } IN hasCollectPointFarmer RETURN NEW) 
          LET newEdge2 = (INSERT { _from: @idCooperative, _to: farmerAdded._id } IN hasCooperativeFarmer RETURN NEW)
          RETURN farmerAdded`, { farmer: body, idCollectPoint: `collectPoint/${idCollectPoint}`, idCooperative: `cooperative/${idCooperative}` }, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(201).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.put('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    let id = req.params.id;
    const validate = Joi.validate(body, schema);

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      if (validate.error) {
        res.status(400).send(validate.error.toString());
      } else {
        db.query(`
          FOR f IN farmer
            FILTER f._id == @idFarmer
              UPDATE f WITH @farmer INTO farmer
                RETURN NEW
        `, {
          idFarmer: 'farmer/' + id,
          farmer: body
        }, function (err, cursor) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            cursor.all(function (err, result) {
              if (err) {
                res.status(500).send('Internal Server Error');
              } else {
                res.status(201).send(result);
              }
            });
          }
        });
      }
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.delete('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id;

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      db.query(`
          LET edgeRemoved = (FOR f, h IN ANY @id hasCollectPoint 
            REMOVE { _key: h._key } IN hasCollectPointFarmer RETURN h)
          LET edgeRemoved2 = (FOR f, h IN ANY @id hasCooperativeFarmer
            REMOVE { _key: h._key } IN hasCooperativeFarmer RETURN h)
          REMOVE { _key: @idFarmer } IN farmer 
            LET farmerRemoved = OLD
            RETURN farmerRemoved`, {idFarmer: id, id: `farmer/${id}`}, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

module.exports = router;
