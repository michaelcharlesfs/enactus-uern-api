var express = require('express');
var router = express.Router();
var db = require('../database');
var guard = require('./guard');
var Joi = require('joi');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

const schema = Joi.object().keys({
  idCollectPoint: Joi.string().required(),
  idFarmer: Joi.string().required(),
  idCooperative: Joi.string().required()
});

const productSchema = Joi.object().keys({
  idProduct: Joi.string().required()
});

router.get('/', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    db.query(`FOR c IN chart RETURN c`, function (err, cursor) {
      if (err) {
        res.status(500).send('Internal Server Error');
      } else {
        cursor.all(function (err, result) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            res.status(200).send(result);
          }
        });
      }
    });
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id;

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN chart FILTER c._id == @idChart RETURN c`, { idChart: `chart/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result[0]);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.post('/', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    const validate = Joi.validate(body, schema);

    if (validate.error) {
      res.status(400).send(validate.error.toString());
    } else {
      db.query(`INSERT @chart INTO chart LET chartAdded = NEW RETURN chartAdded`, { chart: body }, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(201).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.put('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    let id = req.params.id;
    const validate = Joi.validate(body, schema);

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      if (validate.error) {
        res.status(400).send(validate.error.toString());
      } else {
        db.query(`
          FOR c IN chart
            FILTER c._id == @idChart
              UPDATE c WITH @chart INTO chart
                RETURN NEW
        `, {
          idChart: 'chart/' + id,
          chart: body
        }, function (err, cursor) {
          if (err) {
            res.status(500).send('Internal Server Error');
          } else {
            cursor.all(function (err, result) {
              if (err) {
                res.status(500).send('Internal Server Error');
              } else {
                res.status(201).send(result);
              }
            });
          }
        });
      }
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.delete('/:id', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id;

    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if(id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only number.');
    } else {
      db.query(`REMOVE { _key: @idChart } IN chart RETURN OLD`, {idChart: id}, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.post('/:id/products', function(req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let body = req.body;
    let id = req.params.id;
    const validate = Joi.validate(body, productSchema);

    if (validate.error) {
      res.status(400).send(validate.error.toString());
    } else {
      db.query(`
        INSERT { _from: @idChart, _to: @idProduct } INTO hasChartProduct 
          LET edge = NEW 
          RETURN edge`, { idChart: `chart/${id}`, idProduct: `product/${body.idProduct}` }, function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(201).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});

router.get('/:id/products', function (req, res, next) {
  let response = guard.authorizate(req.headers['x-access-token'], 1);
  if (response.auth) {
    let id = req.params.id; 
    if (id === 'undefined') {
      res.status(400).send('ValidationError: Key can not be undefined or empty.');
    } else if (id.match(/^[0-9]*$/) == null) {
      res.status(400).send('ValidationError: Key should contain only numbers.');
    } else {
      db.query(`FOR c IN ANY @idChart hasChartProduct RETURN c`, { idChart: `chart/${id}`} , function (err, cursor) {
        if (err) {
          res.status(500).send('Internal Server Error');
        } else {
          cursor.all(function (err, result) {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send(result);
            }
          });
        }
      });
    }
  } else {
    res.status(401).send('Not authorizated');
  }
});


module.exports = router;
